<?php 

class Ando {

function __construct() {
//echo "<p>". __CLASS__ ." Class construct </p>";
}

function __destruct() {
//echo "<p>".__CLASS__ . " Class destruct </p>";
}
    
public function hello(){
echo " ".__CLASS__ ." Class ";
}
    
public function call_class($name){
	   
if ($name=="oo"){
$obj = new Oo();
}
if ($name=="application"){
$obj = new Application();
}
if ($name=="utility"){
$obj = new Utility();
}
if ($name=="omega"){
$obj = new Omega();
}
return $obj;
}
    
}

class Omega extends Alpha{

function __construct() {
// echo "<p>". __CLASS__ ." Class construct </p>";
}

function __destruct() {
// echo "<p>".__CLASS__ . " Class destruct </p>";
}
    
public function hello(){
echo " ".__CLASS__ ." Class ";
}
    
}

class Alpha{

function __construct() {
//echo "<p>". __CLASS__ ." Class construct </p>";
}

function __destruct() {
//echo "<p>".__CLASS__ . " Class destruct </p>";
}
    
public function hello_alpha(){
echo " ".__CLASS__ ." Class ";
}

public function normalize_files_array($files = []) {

    $arr = [];

    foreach ($files['name'] as $index => $filename) {
        $arr[] = array(
            'name' => $filename,
            'tmp_name' => $files['tmp_name'][$index],
            'error' => $files['error'][$index],
            'size' => $files['size'][$index],
            'type' => $files['type'][$index]
        );
    }

    return $arr;
}

public function upload_image_file($files){
$target_dir = "upload/";

$strtotime = strtotime("now");
$filename = $strtotime.'_'.$_FILES['fileToUpload']['name'];
//$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$target_file = $target_dir . basename($filename);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename($filename). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

}
    
}

class Database {

function __construct() {
//echo "<p>". __CLASS__ ." Class construct </p>";
}

function __destruct() {
//echo "<p>".__CLASS__ . " Class destruct </p>";
}
    
public function hello(){
echo " ".__CLASS__ ." Class ";
}
    
private $host = "localhost";
private $user = "";
private $pass = "";
private $database ="";

private $link;
protected $result;

public function mysqli_db_connect(){
$this->link = mysqli_connect($this->host, $this->user, $this->pass, $this->database);
if (mysqli_connect_errno($this->link)) {
echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
return $mysqli=$this->link;
}

public function mysqli_db_query($sql,$link){
$this->result = mysqli_query($link, $sql);
if(!$this->result) echo "Query failed! ";
if (mysqli_insert_id()){
return mysqli_insert_id();
}
return $this->result;
}

public function count_results($result){
return mysqli_num_rows($result);
}

public function array_result($data){
return mysqli_fetch_array($data);
}

public function object_result($value){
return mysqli_fetch_object($value);
}

public function mysqli_db_close(){
mysqli_close($this->link);
mysqli_free_result($this->result);
}

}

class Utility {

function __construct() {
//echo "<p>". __CLASS__ ." Class construct </p>";
}

function __destruct() {
// echo "<p>".__CLASS__ . " Class destruct </p>";
}
    
public function hello(){
echo " ".__CLASS__ ." Class ";
}

}

class Application {

private $ucon;

function __construct() {
//echo "<p>". __CLASS__ ." Class construct </p>";
}

function __destruct() {
//echo "<p>".__CLASS__ . " Class destruct </p>";
}

public function hello(){
echo " ".__CLASS__ ." Class ";
}
    
public function db(){
return $this->ucon= new Database();
}
    
public function db_connect($conn){
$ucon= 	$this->ucon=$conn;
return $mysqli=$this->ucon->mysqli_db_connect();
    }
    
public function db_close(){
$this->ucon->mysqli_db_close();  
unset($this->ucon); 
} 
    
public function query_result(){
$ucon=$this->db();
$mysqli=$this->db_connect($ucon);	
$sql="SELECT * FROM auto";
$data= $ucon->mysqli_db_query($sql,$mysqli); 
return $data;
}
	
public function results($data){
$row = $this->ucon->array_result($data);
return $row;
}
	
public function num($results){
$count=$this->ucon->count_results($results);
return $count;
}

public function print_result_while(){
$result=$this->query_result();
while ($row = mysqli_fetch_assoc($result)) {
echo "while ".$row['designation']." "."<br/> ";
}

}

public function print_result_for(){
$auto= $this->query_result();
$num= $this->num($auto);
for($n = 0; $n < $num; $n++){
$row=$this->results($auto);
echo "For: ".$n." ".$row['designation']."<br/>";
}
}

public function print_result_foreach(){
$auto= $this->query_result();
foreach ($auto as $data){
echo "foreach ".$data['designation']."<br/>";
}
}	  

}

?>